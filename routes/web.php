<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('gambar');
});

Route::get('/film', function() {
    return view('film');
});

Route::get('film/create', function () {
    return view('film.create');
});

Route::get('/film/create', 'filmController@create');
Route::post('/film', 'filmController@store');

Route::get('film/{id}', function () {
    return view('film.create');
});

Route::get('film/{id}/edit', function () {
    return view('film.create');
});

Route::put('film/{id}', 'filmController@put');
Route::delete('film/{id}', 'filmController@delete');